module FormHelper

  def products_for_select_by_category(category = nil, sel = nil)
    return [] if category.blank?
    category = "other" if category.blank?
    ind_options = ClubceramicItem.where("tile_type = '#{category}'")

  end

  def remote_request(type, path, params={}, target_tag_id)
    "$.#{type}('#{path}',
             {#{params.collect { |p| "#{p[0]}: #{p[1]}" }.join(", ")}},
             function(data) {$('##{target_tag_id}').html(data);}
   );"
  end

end
