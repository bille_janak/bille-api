class FiledataController < ApplicationController

  def index
    prod_key = params[:pro_key]
    ereceipts = Ereceipt.where(pos_product_key: prod_key)

    if ereceipts.blank?
     file_name = nil
     file_path = nil
    else
      prod_path = ProductKey.where(:product_key => prod_key).last
      ereceipt = ereceipts.last
      file_name = ereceipt.file_name
      file_path = prod_path.file_path
      full_file_path = "www.billepro.com" + file_path.gsub("/home/billepro/public_html","") + "/" + file_name
    end
    render json: {file_name: file_name,  file_path: file_path, full_file_path: full_file_path }
  end

  public
  def last_receipt_info
    prod_key = params[:pro_key]
    ereceipts = Ereceipt.where(pos_product_key: prod_key)

    if ereceipts.blank?
      file_name = nil
      file_path = nil
    else
      prod_path = ProductKey.where(:product_key => prod_key).last
      ereceipt = ereceipts.last
      file_name = ereceipt.file_name
      file_path = prod_path.file_path
      full_file_path = "www.billepro.com" + file_path.gsub("/home/billepro/public_html","") + "/" + file_name
      total_amount = ereceipt.total_amount
    end
    render json: {file_name: file_name,  file_path: file_path, full_file_path: full_file_path, total_amount: total_amount }
  end

  def user_transactions
    user_id = params[:user_id]
    user = User.find(3) # User.find(user_id)
    user_transactions = user.ereceipts.where("scanned = 1").joins("left join product_keys p on pos_product_key=p.product_key").joins("left join users u on p.user_id = u.id").select("ereceipts.id, u.store_name, total_amount").order('id desc')
    render json: user_transactions
  end

  def user_recent_transactions
    user_id = params[:user_id]
    user = User.find(3) # User.find(user_id)
    user_transactions = user.ereceipts.where("scanned = 1").joins("left join product_keys p on pos_product_key=p.product_key").joins("left join users u on p.user_id = u.id").select("ereceipts.id, u.store_name, total_amount").order('id desc').limit(10)
    render json: user_transactions
  end

  def receipt_info_by_id
    id = params[:ereceipt_id]
    ereceipt = Ereceipt.where(id: id).last
    prod_key =  ereceipt.pos_product_key

    if ereceipt.blank?
      file_name = nil
      file_path = nil
    else
      prod_path = ProductKey.where(:product_key => prod_key).last
      file_name = ereceipt.file_name
      file_path = prod_path.file_path
      full_file_path = "www.billepro.com" + file_path.gsub("/home/billepro/public_html","") + "/" + file_name
      total_amount = ereceipt.total_amount
    end
    render json: {file_name: file_name,  file_path: file_path, full_file_path: full_file_path, total_amount: total_amount }
  end
end
