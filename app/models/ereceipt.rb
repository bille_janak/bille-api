class Ereceipt < ApplicationRecord

  has_one :product_key, :primary_key => 'pos_product_key', :foreign_key => 'product_key'

  scope :currnet_month, -> { where("scanned = 1 and created_on > ? and created_on < ?", Time.now.beginning_of_month, Time.now.end_of_month)}
  scope :monthly, ->(year_number, month_number) { where("scanned = 1 and extract(year from created_on) = ? and extract(month from created_on) = ?", year_number, month_number)}
  scope :scanned, -> {where("scanned = 1")}

  scope :green_or_paper, -> {where("scanned = 1 or manual_scanned = 1")}
  scope :by_cat_and_sub, ->(category_id, subcategory_id) {where("category_id = ? and sub_category_id = ?",category_id, subcategory_id)}
  scope :not_scanned, -> {where("scanned = 0 or scanned IS NULL")}
  scope :current_receipts, -> { where("created_on > ? ",  61.minutes.ago)}

  scope :by_product_key, -> {joins("left join product_keys p on pos_product_key=p.product_key").joins("left join users u on p.user_id = u.id")}

  scope :by_category, ->(category_id) {where("category_id = ?",category_id)}
  scope :by_subcategory, ->(subcategory_id) {where("sub_category_id = ?",subcategory_id)}

  mount_base64_uploader :image, ImageUploader, file_name: ->  (ereceipt){ereceipt.file_name}

  TOTAL_LABELS = ['TOTAL']
  APP_URL = 'http://billeapi.com/'

  def mark_scanned_by(user_id)
    self.scanned = 1
    self.scanned_by = user_id
    self.scanned_on = Time.now
    self.save
  end

  def get_store_name
    if self.scanned == 1 && self.pos_product_key != nil
      pos_key = self.pos_product_key
      store_user = ProductKey.where("product_key = '#{pos_key}'").last.user
      store_name = store_user.store_name
    else
      store_name = self.manual_scan_store_name
    end
    return store_name
  end

  def self.get_lastreceipt_info(prod_key, current_receipt)
    prod_path = ProductKey.where(:product_key => prod_key).last
    ereceipt = current_receipt
    receipt_id = ereceipt.id
    file_name = ereceipt.file_name
    file_path = prod_path.file_path
    int_filepath = file_path.gsub("/home/app/bille_api/public","")
    full_file_path = "http://billeapi.com/" + int_filepath + "/" + file_name
    if ereceipt.file_data.blank? && ereceipt.total_amount.blank?
      file_data, total_amount, tax_amount = Ereceipt.parse_receipt_info(ereceipt, file_path + "/" + file_name)
    else
      total_amount = ereceipt.total_amount
    end

    logo_path = APP_URL + prod_path.logo_path
    return receipt_id, file_name, file_path, full_file_path, total_amount, logo_path
  end

  def self.parse_receipt_info(ereceipt, file)
    begin
      reader = PDF::Reader.new(file)
      file_data = ""
      reader.pages.each do |page|
        file_data << page.text
      end

      return "", 0, 0 if file_data.blank?
      total_amount = 0
      tax_amount = 0
      s1 = ""
      prod_key         = ereceipt.pos_product_key
      prod_key_pos     = ProductKey.find_by_product_key(prod_key)
      store_user       = prod_key_pos.user
      tax_label        = store_user.tax_label
      total_label      = store_user.total_label
      file_data.each_line do |li|
        if (li[/#{tax_label}\b/])
          s1 = li.chomp
          tax_amount = s1.gsub(/[^0-9,\.]/, '').to_f if !s1.nil?
        end
        if (li[/#{total_label}\b/])
          s1 = li.chomp
          total_amount = s1.gsub(/[^0-9,\.]/, '').to_f if !s1.nil?
        end
      end
      ereceipt.file_data = file_data
      ereceipt.total_amount = total_amount
      ereceipt.tax_amount = tax_amount
      ereceipt.save

      return file_data, total_amount, tax_amount
    rescue StandardError => e
      return "",0,0
    end
  end

  def self.add_scanned_receipt(user_id, file_name, base64_image)
    ereceipt = Ereceipt.new
    ereceipt.file_name = file_name
    ereceipt.manual_scanned = 1
    ereceipt.scanned_by = user_id
    ereceipt.image = base64_image
    content_type = ereceipt.image.content_type.gsub("image/","" )
    ereceipt.file_name = file_name + "." + content_type
    #ereceipt.save!
    file_data, store_name, amount = ereceipt.parse_receipt_image
    ereceipt.file_data = file_data
    ereceipt.total_amount = amount
    ereceipt.manual_scan_store_name = store_name
    ereceipt.category_id = Category.find_by_category_type('Misc Expenses').id
    ereceipt.save!
  end

  def self.add_image_multipart(user_id, file_name, image_file)
    ereceipt = Ereceipt.new
    ereceipt.file_name = file_name
    ereceipt.manual_scanned = 1
    ereceipt.scanned_by = user_id
    ereceipt.image = image_file
    content_type = ereceipt.image.content_type.gsub("image/","" )
    ereceipt.file_name = file_name + "." + content_type
    file_data, store_name, amount = ereceipt.parse_receipt_image rescue nil
    ereceipt.file_data = file_data
    ereceipt.total_amount = amount || 0.0
    ereceipt.manual_scan_store_name = store_name || ""
    cat_id, cat_name = StoreCategory.get_store_category(store_name)
    ereceipt.category_id = cat_id
    ereceipt.save!
    return ereceipt.id, store_name, amount, cat_id
  end

  def parse_receipt_image
    file_name = self.image.file.file
    image = RTesseract.new(file_name)
    file_data = image.to_s
    return "", "", 0 if file_data.blank?
    file_data_array = file_data.split(/\n+/)
    store_name = file_data_array[0] #file_data.lines.first.chomp.first(150)
    amount = 0
    total_amount_label = ['TOTAL','Total']
    s1 = ""

    file_data_array.each { |fd|
      if total_amount_label.any? { |word| fd.include?(word) }
        s1 = fd.chomp
        amount = s1.gsub(/[^0-9,\.]/, '').to_f if !s1.nil?
      end
    }
    #
    # file_data.each_line do |li|
    #   if (li[/#{total_amount_label}\b/])
    #     s1 = li.chomp
    #     amount = s1.gsub(/[^0-9,\.]/, '').to_f if !s1.nil?
    #   end
    # end
    return file_data, store_name, amount
  end

  def self.update_receipt_info(receipt_id, params)
    e = Ereceipt.find(receipt_id)
    is_name_editable = e.manual_scanned
    if is_name_editable
      e.manual_scan_store_name = params[:store_name]
      e.total_amount           = params[:amount]
    else
      e.total_amount           = params[:amount]
    end
    e.save
  end

  def self.is_last_receipt(prod_key)
    receipts = Ereceipt.where(pos_product_key: prod_key)
    if receipts.blank?
      return false, nil
    elsif current_receipt = receipts.not_scanned.current_receipts.last
      return true, current_receipt
    else
      return false, nil
    end
  end

  def self.add_manual_test_receipt(prod_key)
    prod_key = prod_key
    receipt = ['r1.pdf', 'r2.pdf','r3.pdf'].sample
    p = ProductKey.find_by_product_key(prod_key)
    cat_id     =  p.user.category_id || Category.misc_exp.id
    sub_cat_id = p.user.sub_category_id
    ereceipt = Ereceipt.new
    ereceipt.pos_product_key = prod_key
    file_name       = Time.now.to_i.to_s + receipt
    ereceipt.file_name       = file_name
    if receipt == 'r1.pdf'
      FileUtils.cp('public/ereceipts/test_receipts/r1.pdf', "public/ereceipts/28/28-1-1/#{file_name}")
      image_file = 'ereceipts/test_receipts/r1.pdf'
      receipt_data = get_file_as_string('db/r1.txt')
      total_amount = 480.11
    elsif receipt == 'r2.pdf'
      FileUtils.cp('public/ereceipts/test_receipts/r2.pdf', "public/ereceipts/28/28-1-1/#{file_name}")
      image_file = 'ereceipts/test_receipts/r2.pdf'
      receipt_data = get_file_as_string('db/r2.txt')
      total_amount = 176.75
    else
      FileUtils.cp('public/ereceipts/test_receipts/r3.pdf', "public/ereceipts/28/28-1-1/#{file_name}")
      image_file = 'ereceipts/test_receipts/r3.pdf'
      receipt_data = get_file_as_string('db/r3.txt')
      total_amount = 45.99
    end

    ereceipt.file_data       = receipt_data
    ereceipt.tax_amount      = 0
    ereceipt.total_amount    = total_amount
    ereceipt.category_id     = cat_id
    ereceipt.sub_category_id = sub_cat_id
    ereceipt.save(validate:false)
  end

  def self.get_file_as_string(filename)
    data = ''
    f = File.open(filename, "r")
    f.each_line do |line|
      data += line
    end
    return data
  end

  def self.add_new_receipt(params)
    if params[:file_data]
      file_data = params[:file_data].gsub("#","")
    end
    prod_key = params[:pro_key]
    p = ProductKey.find_by_product_key(prod_key)
    cat_id     =  p.user.category_id || Category.misc_exp.id
    sub_cat_id = p.user.sub_category_id
    ereceipt = Ereceipt.new
    ereceipt.pos_product_key = prod_key
    ereceipt.file_name       = params[:file_name]
    ereceipt.file_size       = params[:file_size]
    ereceipt.file_data       = file_data || params[:file_data]
    ereceipt.content_type    = params[:content_type]
    ereceipt.tax_amount      = params[:tax_amount] || 0
    ereceipt.total_amount    = params[:total_amount] || 0
    ereceipt.category_id     = cat_id
    ereceipt.sub_category_id = sub_cat_id
    ereceipt.save(validate:false)
  end

  def self.update_receipt_info(params)
    user_id = params["user_id"]
    receipt_id   = params["receipt_id"]
    return if receipt_id.nil?
    e = Ereceipt.find(receipt_id)
    e.manual_scan_store_name = params["store_name"]
    e.total_amount           = params["total_amount"].to_f
    e.category_id            = params["category_id"]
    e.scanned = 1
    e.scanned_on = Time.now
    e.save
  end

end
