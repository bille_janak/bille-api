class StoreCategory < ApplicationRecord

  belongs_to :category
  belongs_to :sub_category

  scope :exact_store, ->(store_name) {where("store_name = ?","#{store_name}")}
  scope :matching_stores, ->(store_name) {where("store_name like ?","%#{store_name}%")}


  def self.get_store_category(store_name)
    store_name = store_name
    misc_exp_id   = Category.other_exp.id
    misc_exp_name = Category.other_exp.category_type

    if store_name.nil?
      return misc_exp_id, misc_exp_name
    else
      ms = self.exact_store(store_name).last || self.matching_stores(store_name).last
      if ms.blank?
        return misc_exp_id, misc_exp_name
      else
        cat_id     = ms.category_id
        sub_cat_id = ms.sub_category_id
        cat_name   = Category.cat_name(cat_id)
        sub_cat_name = SubCategory.sub_cat_name(sub_cat_id)
        return cat_id, cat_name
      end
    end
  end

end
