class MonthlyExpense < ApplicationRecord

  scope :this_month, -> {where("year = ? and month = ?", Date.today.year, Date.today.month)}

  def self.create_new(u, amount)
    m_exp = u.monthly_expenses.new
    m_exp.expected_expense = amount
    m_exp.month = Date.today.month
    m_exp.year = Date.today.year
    m_exp.save
  end

  def self.update_amount(u,amount)
    this_month_expense = u.monthly_expenses.this_month
    if this_month_expense.exists?
      m_exp = this_month_expense.last
      m_exp.expected_expense = amount
      m_exp.save
    else
      create_new(u, amount)
    end

  end
end
