class SubCategory < ApplicationRecord
  scope :by_category, ->(category_id) {where("category_id = #{category_id}")}

  scope :sub_cat_name, ->(sub_cat_id) {where("id = ?",sub_cat_id).last.sub_category}
end
