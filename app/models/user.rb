class User < ApplicationRecord

  has_many :ereceipts, :foreign_key => :scanned_by
  has_many :journals
  has_many :monthly_expenses, :foreign_key => :user_id

  scope :merchants_all, -> {self.where("register_as = '#{'merchant'}'")}

  # scope :scanned_receipts, self.ereceipts.where("scanned = 1")

  validates_uniqueness_of :email

  def self.get_invoice_labels(user_id)
    user = User.where("email = '#{user_id}'").last
    return user.tax_label , user.total_label
  end

  def self.register_user(params)
    email    = params["email"]
    password = params["password"]
    first_name = params["first_name"]
    last_name  = params["last_name"]
    dob        = params["dob"]
    gender     = params["gender"]
    monthly_exp = params["monthly_expense"]

    return false if User.where("email = '#{email}'").exists?

    u = User.new
    u.email     = email
    u.password  = password
    u.first_name= first_name
    u.last_name = last_name
    u.dob       = dob
    u.register_as = "consumer"
    u.gender      = gender
    u.monthly_expense = monthly_exp
    u.save
    MonthlyExpense.create_new(u, monthly_exp)
    return true, u
  end

  def self.update_user(params)
    user_id = params["user_id"]
    email    = params["email"]
    password = params["password"]
    first_name = params["first_name"]
    last_name  = params["last_name"]
    dob        = params["dob"]
    gender     = params["gender"]
    monthly_exp = params["monthly_expense"]

    return false if user_id.blank?

    u = User.find(user_id) rescue nil

    #return false if User.where("email = '#{email}'").exists? && User.where("email = '#{email}'").last.id != user_id

    u.email     = email
    u.password  = password
    u.first_name= first_name
    u.last_name = last_name
    u.dob       = dob
    u.gender      = gender
    u.monthly_expense = monthly_exp
    u.save
    MonthlyExpense.update_amount(u, monthly_exp)
    return true, u
  end

end
