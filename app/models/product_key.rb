class ProductKey < ApplicationRecord

  scope :keys, -> { where() }

  belongs_to :user

  def self.is_valid?(prod_key)
    prod_key = self.where(product_key: prod_key).last
    return false if prod_key.blank? || prod_key.expired?
    return true
  end

  def self.get_file_path(prod_key)
    prod_key = self.where(product_key: prod_key).last
    return nil if prod_key.blank? || prod_key.expired?
    return prod_key.file_path
  end

  def expired?
    end_date.presence.to_date < Date.today
  end

  def get_category

  end

end
