class Category < ApplicationRecord

  scope :misc_exp, ->{where("category_type = 'Misc Expenses'").last}
  scope :other_exp, ->{where("category_type = 'Other'").last}
  scope :cat_name, ->(cat_id) {where("id = ?",cat_id).last.category_type}

  def self.all_categories
    Category.all.pluck(:id,:category_type)
  end


end
