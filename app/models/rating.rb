class Rating < ApplicationRecord
  def self.create_new_ratings(params)
    return false if params.blank?
    receipt_id = params[:receipt_id]
    customer_id = params[:user_id]
    user_rating = params[:ratings_star]
    customer_comments = params[:comments]
    return false if receipt_id.nil? || customer_id.nil?
    ereceipt = Ereceipt.find(receipt_id)
    store_id = ereceipt.product_key.user_id

    Rating.create({:receipt_id => receipt_id, :customer_id => customer_id, :store_id => store_id, :ratings => user_rating, :comments => customer_comments})
    return true
  end
end
