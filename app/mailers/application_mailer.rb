class ApplicationMailer < ActionMailer::Base
  default from: 'serviceeebill@gmail.com'
  layout 'mailer'

  def send_email(user, password)
    @user = user
    @password = password
    mail(to: @user.email, subject: 'Bill-E Password Reset')
  end

end
