class ClubceramicInvoiceController < ActionController::Base

  public
  def display
    @floor_tiles = ClubceramicItem.where("tile_type = 'floor'")
    render
  end

  public
  def new
    @floor_tiles = ClubceramicItem.where("tile_type = 'floor'")
    render
  end

  def products_for_select_by_category(category = nil, sel = nil)
    return [] if category.blank?
    category = "other" if category.blank?
    ind_options = ClubceramicItem.where("tile_type = '#{category}'")
    options_for_select(ind_options,sel)
  end


  public
  def get_items_by_type
    @items = ClubceramicItem.where("tile_type = '#{params[:tile_type]}'").pluck(:item_name)
    render :json => @items.to_json
  end

end
