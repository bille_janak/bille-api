class UserLoginController < ApplicationController

  def index
    # return {:id => 1}
    user_id = params[:username]
    pwd = params[:password]
    user_id = nil
    if user_id.nil? || User.where("user_name = '#{user_id}' && password = '#{pwd}'").blank?
      user_status = "false"
    else
      user = User.where("user_name = '#{user_id}' && password = '#{pwd}'").last
      user_status = "true"
      user_id = user.id
    end

    render json: {status: user_status, user_id: user_id}
  end

  public
  def validate_user
    user_id = params['username']
    pwd = params['password']
    if user_id.nil? || User.where("email = '#{user_id}' && password = '#{pwd}'").blank?
      user_status = "false"
    else
      user = User.where("email = '#{user_id}' && password = '#{pwd}'").last
      user_status = "true"
      user_id = user.id
    end

    render json: {status: user_status, user_id: user_id}
  end

  def user_details
    user_id = params[:user_id]
    return {} if user_id.nil?
    u = User.find(user_id)
    render json: {user_name: u.user_name, first_name: u.first_name, last_name: u.last_name,
                  email: u.email, dob: u.dob, gender: u.gender, monthly_expense: u.monthly_expense}
  end

  def forgot_password
    user_id = params['username']
    if user_id.nil? || User.where("email = '#{user_id}'").blank?
      user_status = "false"
    else
      user = User.where("email = '#{user_id}'").last
      password = user.password
      ApplicationMailer.send_email(user, password).deliver
    end
  end

  def show
    user_id = params[:username]
    pwd = params[:password]
    user_id = nil
    if user_id.nil? || User.where("user_name = '#{user_id}' && password = '#{pwd}'").blank?
      user_status = "false"
    else
      user = User.where("user_name = '#{user_id}' && password = '#{pwd}'").last
      user_status = "true"
      user_id = user.id
    end

    render json: {status: user_status, user_id: user_id}
  end

  public
  def register_user
    status, u = User.register_user(params)
    render json: {status: status, user_id: u.id}
  end

  def update_user
    status, u = User.update_user(params)
    render json: {status: status, user_id: u&.id}
  end

  def get
  end

  def json_response(object, status = :ok)
    render json: object, status: status
  end

end
