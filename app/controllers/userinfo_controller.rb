class UserinfoController < ApplicationController

  def store_names
    store_names = User.merchants_all.uniq.pluck(:id,:store_name)
    render json: store_names
  end

  def invoice_labels
    user_id = params[:userid]
    tax_label = ""
    total_label = ""
    if user_id.nil? || User.where("email = '#{user_id}'").blank?
      user_status = "false"
    else
      user_status = "true"
      tax_label, total_label = User.get_invoice_labels(user_id)
    end

    render json: {status: user_status, tax_label: tax_label, total_label: total_label}
  end

end
