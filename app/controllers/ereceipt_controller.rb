class EreceiptController < ApplicationController

  def add_receipt
    begin
      Ereceipt.add_new_receipt(params)
      render json: {message: "successfully added", status: "ok"}
    rescue StandardError => e
      render json: {
          error: e.to_s, status: "error"
      }
    end
  end
end
