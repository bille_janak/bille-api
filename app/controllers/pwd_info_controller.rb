class PwdInfoController < ApplicationController
  def get_sftp_credential
    begin
      secrete_key = params[:secrete_key]
      if secrete_key == 'BilleMsPwd@2018'
        pwdinfo = PwdInfo.where("pwdtype = 'sftp'").last
        render json: {status: "ok", url: pwdinfo.url, username: pwdinfo.username, password: pwdinfo.password}
      else
        render json: {status: "ok", message: "invalid secret key"}
      end

    rescue StandardError => e
      render json: {
        error: e.to_s, status: "error"
      }
    end
  end
end
