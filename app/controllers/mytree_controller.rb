class MytreeController < ApplicationController

  public
  def mytree_percentage
    user_id = params[:user_id]
    user = User.find(user_id)
    user_receipts = user.ereceipts.where("scanned = 1")&.count
    mytree_percentage = (user_receipts*2).to_f
    render json: {mytree_percentage: mytree_percentage}
  end

end
