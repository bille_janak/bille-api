class CustomerReviewController < ApplicationController

  public

  def review
    status = Rating.create_new_ratings(params)
    render json: {status: status }
  end

end
