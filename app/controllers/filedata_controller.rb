class FiledataController < ApplicationController
  APP_URL = 'http://billeapi.com/'

  public
  def last_receipt_info
    begin
      prod_key = params[:pro_key]
      if  prod_key == "28-1-1"
        Ereceipt.add_manual_test_receipt(prod_key)
      end
      last_receipt_avail, last_receipt = Ereceipt.is_last_receipt(prod_key)
      if !last_receipt_avail
        render json: {message: "No Receipt Found. Please contact Bill-E for more info!", status: "ok"}
      else
        receipt_id, file_name, file_path, full_file_path, total_amount, logo_path =  Ereceipt.get_lastreceipt_info(prod_key, last_receipt)
        render json: {receipt_id: receipt_id, file_name: file_name,  file_path: file_path, full_file_path: full_file_path, total_amount: total_amount, logo_path: logo_path}
      end
    rescue StandardError => e
      render json: {
          error: e.to_s, status: "error"
      }
    end
  end

  def get_filepath
    begin
      prod_key = params[:prod_key]
      message  = 'ok'
      is_valid = ProductKey.is_valid?(prod_key)
      message  = "Your Product Key is Not Valid. Please contact Bill-E for more info." if !is_valid
      filepath = ProductKey.get_file_path(prod_key)
      render json: {valid: is_valid, file_path: filepath, message: message, status: "ok"}
    rescue StandardError => e
      render json: {
          error: e.to_s, status: "error"
      }
    end
  end


  def validate_product_key
    begin
      prod_key = params[:prod_key]
      message  = 'ok'
      is_valid = ProductKey.is_valid?(prod_key)
      message  = "Your Product Key is Not Valid. Please contact Bill-E for more info." if !is_valid
      render json: {valid: is_valid, message: message, status: "ok"}
    rescue StandardError => e
      render json: {
          error: e.to_s, status: "error"
      }
    end
  end

  def edit_receipt_info
    begin
      receipt_id = params[:receipt_id]
      return false if receipt_id.blank?
      response = true
      error = ""
      Ereceipt.update_receipt_info(receipt_id, params)
      render json: {response: response, error: error}
    rescue ActiveRecord::RecordNotFound
      error = "Record Not Found"
      render json: {
          error: error, status: "error"
      }
    rescue ActiveRecord::ActiveRecordError
      error = "Active Record Error"
      render json: {
          error: error, status: "error"
      }
    rescue StandardError => e
      render json: {
          error: e.to_s, status: "error"
      }
    end
  end

  def scan_receipt
    receipt_id = params[:receipt_id]
    user_id = params[:user_id]
    if (receipt_id.nil? || user_id.nil?)
      render json: {response:false, error:"user or receipt id is nil"}
      return
    end
    receipt = Ereceipt.find(receipt_id) rescue nil
    if receipt.blank?
      render json: {response:false, error:"receipt id not found"}
      return
    end
    receipt.mark_scanned_by(user_id)
    last_receipt = Ereceipt.find(receipt_id)
    prod_key     = last_receipt.pos_product_key
    receipt_id, file_name, file_path, full_file_path, total_amount, logo_path =  Ereceipt.get_lastreceipt_info(prod_key, last_receipt)
    render json: {response:true, error:"", receipt_id: receipt_id, file_name: file_name,  file_path: file_path, full_file_path: full_file_path, total_amount: total_amount, logo_path: logo_path}
  end

  def user_transactions
    user_id = params[:user_id]
    user = User.find(user_id) rescue nil # User.find(user_id)
    return {} if user.nil?
    user_transactions = user.ereceipts.scanned.by_product_key.select("ereceipts.id, u.store_name, total_amount, concat('#{APP_URL}',p.logo_path) as logo_url, date(ereceipts.created_on) as receipt_date").order('ereceipts.created_on desc')
    render json: user_transactions
  end

  def user_recent_transactions
    begin
      user_id = params[:user_id]
      user = User.find(user_id) rescue nil  # User.find(user_id)
      return {} if user.nil?
      user_transactions = user.ereceipts.green_or_paper.by_product_key.select("ereceipts.id, coalesce(u.store_name,'Bill-E') as store_name, total_amount, concat('#{APP_URL}',p.logo_path) as logo_url, date(ereceipts.created_on) as receipt_date").order('ereceipts.created_on desc').limit(50)
    rescue Exception
      response = false
      error = "Error!"
    end
    render json: user_transactions
  end

  def receipt_info_by_id
    id = params[:ereceipt_id]
    ereceipt = Ereceipt.where(id: id).last
    prod_key =  ereceipt.pos_product_key

    if ereceipt.blank?
      file_name = nil
      file_path = nil
    elsif ereceipt.manual_scanned
      receipt_image = ereceipt.image
      receipt_image_file = receipt_image.file
      file_name = receipt_image_file.filename
      file_path = receipt_image_file.path
      int_filepath = file_path.gsub("/home/app/bille_api/public","")
      full_file_path = "http://billeapi.com/" + int_filepath
      total_amount = ereceipt.total_amount
    else
      prod_path = ProductKey.where(:product_key => prod_key).last
      file_name = ereceipt.file_name
      file_path = prod_path.file_path
      int_filepath = file_path.gsub("/home/app/bille_api/public","")
      full_file_path = "http://billeapi.com/" + int_filepath + "/" + file_name
      total_amount = ereceipt.total_amount
    end
    render json: {file_name: file_name,  file_path: file_path, full_file_path: full_file_path, total_amount: total_amount }
  end

  def receipt_scan
    begin
      user_id = params[:user_id]
      base64_image = params[:base64_image].to_s
      file_name = user_id.to_s + "_" + Time.now.to_i.to_s
      Ereceipt.add_scanned_receipt(user_id, file_name, base64_image)
      response = true
      error = ""
      render json: {response:response, error:error}
    rescue StandardError => e
      render json: {
          error: e.to_s, status: "error"
      }
    end
  end

  def receipt_upload
    begin
      user_id = params[:user_id]
      image_file = params[:file]
      file_name = user_id.to_s + "_" + Time.now.to_i.to_s
      id, store_name, amount, cat_id = Ereceipt.add_image_multipart(user_id, file_name, image_file)
      response = true
      amount = 0 if amount.nil?
      store_name = "" if store_name.nil?
      all_categories = Category.all_categories
      render json: {response:response, receipt_id:id, store_name:store_name, amount:amount, default_cat_id:cat_id, all_categories:all_categories }
    rescue StandardError => e
      render json: {
          error: e.to_s, status: "error"
      }
    end
  end

  def update_receipt_data
    begin
      params.inspect
      Ereceipt.update_receipt_info(params)
      render json: {response:true}
    rescue StandardError => e
      render json: {
          error: e.to_s, status: "error"
      }
    end
  end

end
