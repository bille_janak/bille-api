class FiledataController < ApplicationController
  APP_URL = 'http://132.148.21.222:2088/'
  def index
    prod_key = params[:pro_key]
    ereceipts = Ereceipt.where(pos_product_key: prod_key)

    if ereceipts.blank?
     file_name = nil
     file_path = nil
    else
      prod_path = ProductKey.where(:product_key => prod_key).last
      ereceipt = ereceipts.last
      file_name = ereceipt.file_name
      file_path = prod_path.file_path
      full_file_path = "www.billepro.com" + file_path.gsub("/home/billepro/public_html","") + "/" + file_name
    end
    render json: {file_name: file_name,  file_path: file_path, full_file_path: full_file_path }
  end

  public
  def last_receipt_info
    prod_key = params[:pro_key]
    ereceipts = Ereceipt.where(pos_product_key: prod_key)

    if ereceipts.blank?
      file_name = nil
      file_path = nil
    else
      prod_path = ProductKey.where(:product_key => prod_key).last
      ereceipt = ereceipts.last
      receipt_id = ereceipt.id
      file_name = ereceipt.file_name
      file_path = prod_path.file_path
      full_file_path = "www.billepro.com" + file_path.gsub("/home/billepro/public_html","") + "/" + file_name
      total_amount = ereceipt.total_amount
      logo_path = APP_URL + prod_path.logo_path
    end
    render json: {receipt_id: receipt_id, file_name: file_name,  file_path: file_path, full_file_path: full_file_path, total_amount: total_amount, logo_path: logo_path }
  end

  def validate_product_key
    prod_key = params[:prod_key]
    return ProductKey.where(product_key: prod_key).exists?
  end

  def scan_receipt
    receipt_id = params[:receipt_id]
    user_id = params[:user_id]
    if (receipt_id.nil? || user_id.nil?)
      render json: {response:false, error:"user or receipt id is nil"}
      return
    end
    receipt = Ereceipt.find(receipt_id) rescue nil
    if receipt.blank?
      render json: {response:false, error:"receipt id not found"}
      return
    end
    receipt.mark_scanned_by(user_id)
    render json: {response:true, error:""}
  end

  def user_transactions
    user_id = params[:user_id]
    user = User.find(user_id) rescue nil # User.find(user_id)
    return {} if user.nil?
    user_transactions = user.ereceipts.scanned.by_product_key.select("ereceipts.id, u.store_name, total_amount, concat('#{APP_URL}',p.logo_path) as logo_url, date(ereceipts.created_on) as receipt_date").order('ereceipts.created_on desc')
    render json: user_transactions
  end

  def user_recent_transactions
    user_id = params[:user_id]
    user = User.find(user_id) rescue nil  # User.find(user_id)
    return {} if user.nil?
    user_transactions = user.ereceipts.scanned.by_product_key.select("ereceipts.id, u.store_name, total_amount, concat('#{APP_URL}',p.logo_path) as logo_url, date(ereceipts.created_on) as receipt_date").order('ereceipts.created_on desc').limit(10)
    render json: user_transactions
  end

  def receipt_info_by_id
    id = params[:ereceipt_id]
    ereceipt = Ereceipt.where(id: id).last
    prod_key =  ereceipt.pos_product_key

    if ereceipt.blank?
      file_name = nil
      file_path = nil
    else
      prod_path = ProductKey.where(:product_key => prod_key).last
      file_name = ereceipt.file_name
      file_path = prod_path.file_path
      full_file_path = "www.billepro.com" + file_path.gsub("/home/billepro/public_html","") + "/" + file_name
      total_amount = ereceipt.total_amount
    end
    render json: {file_name: file_name,  file_path: file_path, full_file_path: full_file_path, total_amount: total_amount }
  end
end
