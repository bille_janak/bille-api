class MyspendAnalysisController < ApplicationController

  public

  def myspends
    begin
      user_id = params[:user_id]
      user = User.find(user_id) rescue nil #User.find(3) #
      if user.nil?
        render json: {
            error: "User not found", status: "error"
        }
      else
        user_receipts = user.ereceipts.scanned.by_product_key
        all_categories = Category.all

        myspends = Array.new
        all_categories.each{ |ac|
          category = ac.category_type
          category_id = ac.id
          category_total = user.ereceipts.scanned.by_category(category_id).sum(:total_amount).to_f
          myspends << {:id => category_id, :name => category.to_s, :total => category_total, :color_code => ac.color_code}
        }
        render json: myspends
      end

    rescue StandardError => e
      render json: {
          error: e.to_s, status: "error"
      }
    end
  end

  def myspends_sub
    begin
      user_id = params[:user_id]
      catetgory_id = params[:category_id]
      user = User.find(user_id) rescue nil #User.find(3)
      return {} if user.nil?
      if user.nil? || catetgory_id.nil?
        render json: {
            error: "User or Category not found", status: "error"
        }
      else
        user_receipts = user.ereceipts.scanned.by_product_key
        all_categories = SubCategory.by_category(catetgory_id)
        myspends_sub = Array.new
        all_categories.each{ |ac|
          sub_category = ac.sub_category
          sub_category_id = ac.id
          sub_category_total = user.ereceipts.scanned.by_subcategory(sub_category_id).sum(:total_amount).to_f
          myspends_sub << {:id => sub_category_id, :name => sub_category.to_s, :total => sub_category_total, :color_code => ac.color_code}
        }
        render json: myspends_sub
      end
    rescue StandardError => e
      render json: {
          error: e.to_s, status: "error"
      }
    end
  end

  def myanalysis_sub_category
    begin
      user_id = params[:user_id]
      catetgory_id = params[:category_id]
      sub_catetgory_id = params[:sub_category_id]
      user = User.find(user_id) rescue nil #User.find(3)
      return {} if user.nil?
      if user.nil? || catetgory_id.nil? || sub_catetgory_id.nil?
        render json: {
            error: "User or Category not found", status: "error"
        }
      else
        sub_user_receipts = user.ereceipts.green_or_paper.by_cat_and_sub(catetgory_id, sub_catetgory_id).last(10)

        myspends_sub_details = Array.new
        sub_user_receipts.each{ |sur|
          store_name = sur.get_store_name
          total      = sur.total_amount
          myspends_sub_details << {:id => sur.id, :name => store_name.to_s, :total => total}
        }
        render json: myspends_sub_details
      end
    rescue StandardError => e
      render json: {
          error: e.to_s, status: "error"
      }
    end
  end

  def myspends_monthly
    begin
      user_id = params[:user_id] || User.find(3)
      month   = params[:month] || Time.now.month
      year    = params[:year] || Time.now.year
      user = User.find(user_id) rescue nil #User.find(3) #
      return {} if user.nil?
      receipts_monthly = user.ereceipts.monthly(year,month)

      return {} if receipts_monthly.blank?
      total_spend_cm =  receipts_monthly.sum(:total_amount).to_f
      total_planned = user.monthly_expense.to_f
      total_remaining = total_planned - total_spend_cm
      days_remaining = Time.now.end_of_month.day - Time.now.day
      render json: {total: total_planned, spend: total_spend_cm, pending: total_remaining, days_remaining: days_remaining}
    rescue StandardError => e
      render json: {
          error: e.to_s, status: "error"
      }
    end

  end

end
