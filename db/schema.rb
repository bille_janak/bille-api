# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180220194909) do

  create_table "EmployeeDetails", unsigned: true, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "EmpId"
    t.string  "FullName"
    t.integer "ManagerId"
    t.date    "DateOfJoining"
  end

  create_table "EmployeeSalary", unsigned: true, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string  "Project", limit: 11
    t.integer "EmpId"
    t.integer "Salary"
  end

  create_table "Projects", unsigned: true, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "projectid"
    t.string "projectname"
  end

  create_table "categories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "category_type"
    t.string   "description"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "clubceramic_invoice_items", unsigned: true, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "invoice_id"
    t.integer "item_id"
    t.float   "num_boxes",   limit: 24
    t.float   "unit_price",  limit: 24
    t.float   "total_sq_ft", limit: 24
    t.float   "total_price", limit: 24
    t.string  "size"
  end

  create_table "clubceramic_invoices", unsigned: true, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.date    "invoice_date"
    t.date    "delivery_date"
    t.string  "delivery_method"
    t.integer "sales_rep"
    t.string  "bill_to_name"
    t.string  "bill_to_address"
    t.string  "bill_to_city"
    t.string  "bill_to_state"
    t.string  "bill_to_zip"
    t.string  "bill_to_phone"
    t.string  "bill_to_email"
    t.string  "ship_to_name"
    t.string  "ship_to_address"
    t.string  "ship_to_city"
    t.string  "ship_to_state"
    t.string  "ship_to_zip"
    t.string  "ship_to_phone"
    t.float   "discount_percent",         limit: 24
    t.float   "subtotal",                 limit: 24
    t.float   "shipping_handling_amount", limit: 24
    t.float   "gst_amount",               limit: 24
    t.float   "qst_amount",               limit: 24
    t.float   "total_amount",             limit: 24
  end

  create_table "ereceipts", unsigned: true, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "pos_id"
    t.string   "pos_product_key",                                                    comment: "store_pos_id from\\rproduct_keys table"
    t.text     "file_name",              limit: 4294967295
    t.string   "file_size"
    t.text     "file_data",              limit: 4294967295,                          comment: "actual data"
    t.string   "content_type"
    t.datetime "created_on"
    t.datetime "updated_on"
    t.datetime "deleted_on"
    t.decimal  "tax_amount",                                precision: 10, scale: 2
    t.decimal  "total_amount",                              precision: 10, scale: 2
    t.integer  "scanned",                limit: 1
    t.integer  "scanned_by"
    t.integer  "category_id"
    t.integer  "sub_category_id"
    t.datetime "scanned_on"
    t.binary   "manual_scanned",         limit: 65535
    t.integer  "manual_scan_store_id"
    t.string   "manual_scan_store_name"
    t.text     "image",                  limit: 4294967295
    t.string   "store_dir"
  end

  create_table "journals", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "user_id"
    t.integer  "created_by"
    t.string   "title"
    t.string   "body"
    t.string   "operation"
    t.string   "data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "monthly_expenses", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "user_id"
    t.string   "expected_expense"
    t.integer  "month"
    t.integer  "year"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "product_keys", unsigned: true, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer  "user_id",     null: false
    t.string   "product_key", null: false
    t.string   "file_path"
    t.string   "logo_path"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.datetime "start_date"
    t.datetime "end_date"
  end

  create_table "pwd_infos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "pwdtype"
    t.string   "url"
    t.string   "ip"
    t.string   "username"
    t.string   "password"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ratings", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "receipt_id"
    t.integer  "customer_id"
    t.integer  "store_id"
    t.float    "ratings",     limit: 24
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.text     "comments",    limit: 4294967295
  end

  create_table "store_categories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "store_name"
    t.integer  "category_id"
    t.integer  "sub_category_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "total_label"
    t.string   "tax_label"
  end

  create_table "sub_categories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "sub_category"
    t.string   "description"
    t.integer  "category_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "users", unsigned: true, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "user_name",                                    comment: "user name (mandatory)"
    t.string   "password",                                     comment: "password (mandatory)"
    t.string   "first_name",                                   comment: "Store name (mandatory)"
    t.string   "last_name",                                    comment: "Branch name (mandatory)"
    t.string   "store_name",                                   comment: "pos id / name (mandatory)"
    t.string   "industry",                                     comment: "Store Industry"
    t.string   "industry_other",                               comment: "Store Industry other (If not any of dropdown list)"
    t.string   "email",                                        comment: "contact person email (optional)"
    t.string   "phone",                                        comment: "contact person phone (optional)"
    t.text     "from_ips",        limit: 65535,                comment: "from ip address of user registration"
    t.datetime "created_on",                                   comment: "datetime when record is created"
    t.datetime "updated_on",                                   comment: "datetime when record is created"
    t.datetime "deleted_on",                                   comment: "flag to mark item deleted (not physical delete)"
    t.string   "tax_label"
    t.string   "total_label"
    t.string   "register_as"
    t.string   "age_group"
    t.integer  "category_id"
    t.integer  "sub_category_id"
    t.datetime "dob"
    t.string   "gender"
    t.decimal  "monthly_expense",               precision: 10
  end

end
