class AddScannedon < ActiveRecord::Migration[5.0]
  def change
    add_column :ereceipts, :scanned_on, :datetime
  end
end
