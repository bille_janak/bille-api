class CreateRatings < ActiveRecord::Migration[5.0]
  def change
    create_table :ratings do |t|
      t.integer :receipt_id
      t.integer :customer_id
      t.integer :store_id
      t.float :ratings
      t.string :comments, :limit => 20000
      t.timestamps
    end
  end
end
