class CreateSubCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :sub_categories do |t|
      t.string :sub_category
      t.string :description
      t.integer :category_id
      t.timestamps
    end

    add_column :users, :category_id, :integer
    add_column :users, :sub_category_id, :integer
  end
end
