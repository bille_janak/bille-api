class CreateJournals < ActiveRecord::Migration[5.0]
  def change
    create_table :journals do |t|
      t.integer :user_id
      t.integer :created_by
      t.string :title
      t.string :body
      t.string :operation
      t.string :data
      t.timestamps
    end
  end
end
