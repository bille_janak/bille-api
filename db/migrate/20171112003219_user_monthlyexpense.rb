class UserMonthlyexpense < ActiveRecord::Migration[5.0]
  def change
    create_table :monthly_expenses do |t|
      t.integer :user_id
      t.string :expected_expense
      t.integer :month
      t.integer :year
      t.timestamps
    end
  end
end
