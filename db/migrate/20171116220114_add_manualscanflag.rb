class AddManualscanflag < ActiveRecord::Migration[5.0]
  def change
    add_column :ereceipts, :manual_scanned, :binary
    add_column :ereceipts, :manual_scan_store_id, :int
    add_column :ereceipts, :manual_scan_store_name, :string
  end
end
