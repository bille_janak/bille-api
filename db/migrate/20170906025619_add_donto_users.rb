class AddDontoUsers < ActiveRecord::Migration[5.0]
  def up
    add_column :users, :dob, :datetime
    add_column :users, :gender, :string
    add_column :users, :monthly_expense, :decimal
  end

  def down
    remove_column :users, :monthly_expense
    remove_column :users, :gender
    remove_column :users, :dob
  end

end
