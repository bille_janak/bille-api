class CreateStoreCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :store_categories do |t|
      t.string :store_name
      t.integer :category_id
      t.integer :sub_category_id
      t.timestamps
    end
  end
end
