class AddTimestampsProdkey < ActiveRecord::Migration[5.0]
  def change
    add_column :product_keys, :created_at, :datetime, null: false
    add_column :product_keys, :updated_at, :datetime, null: false
    add_column :product_keys, :start_date, :datetime
    add_column :product_keys, :end_date, :datetime
  end
end
