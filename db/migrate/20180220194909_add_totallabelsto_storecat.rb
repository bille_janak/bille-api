class AddTotallabelstoStorecat < ActiveRecord::Migration[5.0]
  def up
    add_column :store_categories, :total_label, :string
    add_column :store_categories, :tax_label, :string
  end

  def down
    remove_column :store_categories, :tax_label
    remove_column :store_categories, :total_label
  end

end
