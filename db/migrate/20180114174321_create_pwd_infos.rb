class CreatePwdInfos < ActiveRecord::Migration[5.0]
  def change
    create_table :pwd_infos do |t|
      t.string :pwdtype
      t.string :url
      t.string :ip
      t.string :username
      t.string :password
      t.timestamps
    end
  end
end
