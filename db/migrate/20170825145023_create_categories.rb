class CreateCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :categories do |t|
      t.string :category_type
      t.string :description
      t.timestamps
    end

    Category.create(:category_type => 'Shopping', :description => 'Water,Electricity,Heating etc.')
    Category.create(:category_type => 'Home')
    Category.create(:category_type => 'Auto & Transportation')
    Category.create(:category_type => 'Entertainment', :description => "Museum, Amusment Park etc.")
    Category.create(:category_type => 'Bill & Utilities')
    Category.create(:category_type => 'Food & Dining')
    Category.create(:category_type => 'Health & Fitness', :description => "Health,Pharmacy Stores,Drugs,Doctor Visit etc.")
    Category.create(:category_type => 'Personal Care')
    Category.create(:category_type => 'Education')
    Category.create(:category_type => 'Pets')
    Category.create(:category_type => 'Misc Expenses')
  end
end
