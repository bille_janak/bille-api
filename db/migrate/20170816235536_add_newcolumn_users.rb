class AddNewcolumnUsers < ActiveRecord::Migration[5.0]

  def up
    add_column :users, :age_group, :string
  end

  def down
    remove_column :users, :age_group
  end

end
