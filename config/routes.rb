Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :user_login do
    collection do
      get :validate_user
      get :forgot_password
      get :user_details
      post :register_user
      post :update_user
    end
  end

  resources :ereceipt do
    collection do
      post :add_receipt
    end
  end

  resources :pwd_info do
    collection do
      get :get_sftp_credential
    end
  end

  resources :driveragreement do
    collection do
      get :index
    end
  end

  resources :filedata do
    collection do
      get :last_receipt_info
      get :user_transactions
      get :receipt_info_by_id
      get :user_recent_transactions
      get :validate_product_key
      get :get_filepath
      post :edit_receipt_info
      post :receipt_scan
      post :scan_receipt
      post :receipt_upload
      post :update_receipt_data
    end
  end

  resources :userinfo do
    collection do
      get :store_names
      get :invoice_labels
    end
  end

  resources :clubceramic_invoice do
    collection do
      get :display
      get :new
      get :get_items_by_type
      post :create_invoice
    end
  end

  resources :myspend_analysis do
    collection do
      get :myspends
      get :myspends_sub
      get :myspends_monthly
      get :myanalysis_sub_category
    end
  end

  resources :customer_review do
    collection do
      post :review
    end
  end

  resources :mytree do
    collection do
      get :mytree_percentage
    end
  end
end
